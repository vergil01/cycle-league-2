﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CycleLeagueApp
{
    //D J Mullier 2014
    //Refactored by Thalita 2018
    using System;
    namespace cycleLeague
    {
        public class League
        {
            static Random rnd = new Random();

            public static void Main()
            {
                int numberOfEvents = 18;
                int numberOfCompetitors = 20;
                CycleLeague league = new CycleLeague("Time Trial League", numberOfEvents);

                RunLeague(numberOfEvents, numberOfCompetitors, league);
                league.DisplayCompetitors();

                Console.Read();// press a key to exit at the end
            }

            public static CycleLeague RunLeague(int numberOfEvents, int numberOfCompetitors, CycleLeague league)
            {
                List<Competitor> competitors = CreateCompetitors(league, numberOfCompetitors);

                //for each event, assign random points to all competitors
                foreach (var i in Enumerable.Range(0, numberOfEvents))
                {
                    AssignRandomPoints(competitors);
                }

                //competitors.ForEach(c => c.getPointsEvent(1)); //What is this for?

                league.SortResults();
                AwardPrize(league);
                return league;
            }

            /**
             * Awards a prize for the winner
             * **/
            public static void AwardPrize(CycleLeague league)
            {
                Competitor competitor = league.GetCompetitor(0);
                Award award = new Award("League Champion");
                competitor.addAward(award);
                award = new Award("Hilly 19 Trophy");
                competitor.addAward(award);
                award = new Award("Triangle Trophy");
                competitor.addAward(award);
            }

            /**
             * Assigns random points to a list of competitors.
             * */
            public static void AssignRandomPoints(List<Competitor> competitors)
            {
                foreach (Competitor competitor in competitors)
                {
                    int ran = rnd.Next(50);
                    competitor.setResult(ran); //assign random set of results
                }
            }

            /**
             * Creates a given number of competitors into a Cycle League
             * */
            public static List<Competitor> CreateCompetitors(CycleLeague league, int numberOfCompetitors)
            {
                List<Competitor> competitors = new List<Competitor>();
                foreach (var i in Enumerable.Range(0, numberOfCompetitors)) //loop to create each competitor
                {
                    competitors.Add(new Competitor(league, "Person " + i, "Nova", "Senior"));
                }
                return competitors;
            }
        }
    }

}
